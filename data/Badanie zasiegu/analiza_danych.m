%analiza danych komunikacja 1-1 satelitek
%Pawel Grad

clc;
clear all;

% fileID = fopen('c1 - data.txt','r');
% fileID = fopen('c2 - data.txt','r');
% fileID = fopen('c3 - data.txt','r');
% fileID = fopen('c4 - data.txt','r');
% fileID = fopen('d1 - data.txt','r');
% fileID = fopen('d2 - data.txt','r');
fileID = fopen('d3 - data.txt','r');
lines = textscan(fileID,'%d');
A = lines{1,1};
plot(A, 'b.');
xlabel('Nr pakietu', 'FontSize', 20);
ylabel('Ping [us]', 'FontSize', 20);
% title('Czas odpowiedzi - w pomieszczeniu, 3m', 'FontSize', 24);
% title('Czas odpowiedzi - w pomieszczeniu, 7m, 2 sciany', 'FontSize', 24);
% title('Czas odpowiedzi - w pomieszczeniu, 10m, 2 sciany', 'FontSize', 24);
% title('Czas odpowiedzi - w pomieszczeniu, 3m, strop', 'FontSize', 24);
% title('Czas odpowiedzi - na zewnatrz, 15m', 'FontSize', 24);
% title('Czas odpowiedzi - na zewnatrz, 30m', 'FontSize', 24);
title('Czas odpowiedzi - na zewnatrz, 40m, przeszkody terenowe', 'FontSize', 24);
axis([0 length(A) min(A)-300 max(A)+300])
set(gca,'FontSize',18);

maksimum = max(A)
minimum =  min(A(A>0))
srednia = mean2(A)
mediana = median(A)

lost = sum(A == 0)
success = (length(A)-lost)/length(A)

fclose(fileID);