%analiza danych komunikacja 1-1 satelitek
%Pawel Grad

clc;
clear all;

load('gac1_data.mat');
% gac1data(:,2)
hold on;
plot(gac1data(:,2), 'b');
plot(gac1data(:,5), 'g');
plot(gac1data(:,8), 'r');
legend({'akcelerometr', '�yroskop', 'filtr komplementarny'}, 'FontSize', 18);
xlabel('Nr pr�bki', 'FontSize', 20);
ylabel('Warto�� k�ta [stopnie]', 'FontSize', 20);
title('K�t wychylenia roll - por�wnanie odczyt�w', 'FontSize', 24);
% axis([0 length(A) min(A)-300 max(A)+300])

figure;
hold on;
plot(gac1data(:,3), 'b');
plot(gac1data(:,6), 'g');
plot(gac1data(:,9), 'r');
legend({'akcelerometr', '�yroskop', 'filtr komplementarny'}, 'FontSize', 18);
xlabel('Nr pr�bki', 'FontSize', 20);
ylabel('Warto�� k�ta [stopnie]', 'FontSize', 20);
title('K�t wychylenia pitch - por�wnanie odczyt�w', 'FontSize', 24);
